	ImageROI.y = 0;
	G_to_RGB.copyTo(result(ImageROI));

	ImageROI.x = img_1.cols * 3+30;
	ImageROI.y = 0;
	R_to_RGB.copyTo(result(ImageROI));

	
	return result;
}

int main()
{
    Mat img = imread("../../../testdata/pencils__photo.jpg");
	imshow("pencils__photo.jpg", img);

	Mat gray;
	cvtColor(img, gray, CV_BGR2GRAY);

	vector<int> compression_params;
	compression_params.push_back(IMWRITE_JPEG_QUALITY);
	compression_params.push_back(90);

    imwrite("../../../prj.lab/lab02/pencils_90.jpg", img, compression_params);

	compression_params.pop_back();
	compression_params.push_back(60);
    imwrite("../../../prj.lab/lab02/pencils_60.jpg", img, compression_params);

	Mat img_90 = imread("pencils_90.jpg");
	Mat img_60 = imread("pencils_60.jpg");

	Mat result1=Difference(img, img_90);
	Mat result2 = Difference(img, img_60);

	int height = result1.rows*2;
	int width = result1.cols;
	//картинка для вывода
	Mat result = Mat(height, width, result1.type());

	Rect imageROI = Rect(0, 0, result1.cols, result1.rows);
	
	result1.copyTo(result(imageROI));

	Rect imageROIH = Rect(0, 0, result1.cols, result1.rows);
	imageROIH.x = 0;
	imageROIH.y = result1.rows;

	//добавляем гистограмму
	result2.copyTo(result(imageROIH));
	imshow(" result", result);
	//imwrite("../../../prj.lab/lab03/lab03_image_and_hist.jpg", result);

	//imshow("result.jpg", result1);
    imwrite("../../../prj.lab/lab02/result.jpg", result);

	waitKey(0);
	return 0;
}